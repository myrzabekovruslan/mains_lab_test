from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('client', views.ClientViewSet)
router.register('organization', views.OrganizationViewSet)
# router.register('bill', views.BillViewSet)


urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('', views.HomeView, name='home'),
    path('upload_files/', views.UploadExcelFilesView, name='upload_files'),
    path('clients_list/', views.ClientListView, name='clients_list'),
    path('bills_list/', views.BillListView, name='bills_list'),
    path('api/v1/client_org/', views.post_client_org_xlsx, name='client_org_excel'),
    path('api/v1/bills/', views.post_bills_xlsx, name='bills_excel'),
    path('api/v1/bills_list/', views.get_bills_list, name='bills_list_api'),

]
