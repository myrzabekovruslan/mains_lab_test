from .models import Bill
import django_filters


class BillFilter(django_filters.FilterSet):

    class Meta:
        model = Bill
        fields = ['organization', 'organization__client', ]
