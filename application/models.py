from django.db import models


class Client(models.Model):
    '''Клиент'''
    name = models.CharField(max_length=255, unique=True, verbose_name="Имя")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Клиенты"
        verbose_name = "Клиент"


class Organization(models.Model):
    '''Организация'''
    name = models.CharField(max_length=255, verbose_name="Название")
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name="organizations",
        verbose_name="Клиент",
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Организации"
        verbose_name = "Организация"
        unique_together = (('name', 'client',),)


class Bill(models.Model):
    '''Счет'''
    organization = models.ForeignKey(
        Organization,
        on_delete=models.CASCADE,
        related_name="bills",
        verbose_name="Организация",
    )
    number = models.IntegerField(verbose_name="Номер")
    summa = models.BigIntegerField(verbose_name="Сумма")
    committed_date = models.DateField(verbose_name="Дата создания")

    def __str__(self):
        return f'Organization: {self.organization}; Date: {self.committed_date}'

    class Meta:
        verbose_name_plural = "Счета"
        verbose_name = "Счет"
        unique_together = (('organization', 'number',), )