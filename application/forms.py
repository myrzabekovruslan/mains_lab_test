from .models import Client, Organization, Bill
from django import forms


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = "__all__"


class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = "__all__"


class BillForm(forms.ModelForm):
    class Meta:
        model = Bill
        fields = "__all__"