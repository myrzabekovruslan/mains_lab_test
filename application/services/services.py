from ..models import Client, Organization
from ..forms import ClientForm, OrganizationForm, BillForm
from openpyxl import load_workbook
from django.db import transaction


@transaction.atomic
def save_client_org(clients: list, client_org: dict) -> bool:
    try:
        for client in clients:
            form = ClientForm(data={'name': client})
            if form.is_valid():
                form.save()
            else:
                return False

        for client, organizations in client_org.items():
            for organization in organizations:
                form = OrganizationForm(data={
                    'name': organization,
                    'client': Client.objects.get(name=client).id,
                })
                if form.is_valid():
                    form.save()
                else:
                    return False

        return True
    except Exception as err:
        print(err)
        return False


@transaction.atomic
def save_bills(bills: list) -> bool:
    try:
        for bill in bills:
            form = BillForm(data={
                'organization': Organization.objects.get(name=bill[0]).id,
                'number': bill[1],
                'summa': bill[2],
                'committed_date': bill[3],
            })
            if form.is_valid():
                form.save()
            else:
                return False

        return True
    except Exception as err:
        print(err)
        return False


def extract_data_from_client_org_excel(client_org_file) -> [(list, dict), None]:
    try:
        wb = load_workbook(filename=client_org_file.file)
        client_sheet = wb.active

        client_names = []
        for i in range(2, client_sheet.max_row+1):
            client_names.append(client_sheet.cell(row=i, column=1).value)

        organization_sheet = wb.get_sheet_by_name('organization')
        client_org = dict()
        for i in range(2, organization_sheet.max_row+1):
            # if exist client that append organization in list
            # else create list for client with one value
            if client_org.get(organization_sheet.cell(row=i, column=1).value):
                client_org[organization_sheet.cell(row=i, column=1).value].append(
                    organization_sheet.cell(row=i, column=2).value,
                )
            else:
                client_org[organization_sheet.cell(row=i, column=1).value] = [
                    organization_sheet.cell(row=i, column=2).value,
                ]

        return client_names, client_org
    except Exception as err:
        print(err)


def extract_data_from_bills_excel(bills_file) -> [list, None]:
    try:
        wb = load_workbook(filename=bills_file.file)
        sheet = wb.active
        bills = []
        for i in range(2, sheet.max_row+1):
            bills.append([
                sheet.cell(row=i, column=1).value,
                sheet.cell(row=i, column=2).value,
                sheet.cell(row=i, column=3).value,
                sheet.cell(row=i, column=4).value,
            ])

        return bills
    except Exception as err:
        print(err)


def get_organization_by_id(id: int) -> [Organization, None]:
    try:
        return Organization.objects.get(id=id)
    except Exception as err:
        print(err)


def get_client_by_id(id: int) -> [Client, None]:
    try:
        return Client.objects.get(id=id)
    except Exception as err:
        print(err)