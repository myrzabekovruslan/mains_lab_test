from django.shortcuts import render, redirect
from .models import Client, Bill, Organization
from .services.services import extract_data_from_client_org_excel, save_client_org, \
    extract_data_from_bills_excel, save_bills, get_organization_by_id, get_client_by_id
from django.db.models import Count, Sum
from .filters import BillFilter
from .serializers import ClientSerializer, OrganizationSerializer, BillSerializer
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from django_filters import rest_framework as dj_filter


def HomeView(request):
    return render(request, 'home_page.html')


def UploadExcelFilesView(request):
    if request.POST:
        client_org = request.FILES.get('client_org')
        bills = request.FILES.get('bills')
        if client_org:
            data = extract_data_from_client_org_excel(client_org)
            if not data or not save_client_org(data[0], data[1]):
                return render(request, 'upload_files_page.html',
                              {'error_1': 'Ошибка данных таблицы клиентов'})

        if bills:
            data = extract_data_from_bills_excel(bills)
            if not data or not save_bills(data):
                return render(request, 'upload_files_page.html',
                              {'error_2': 'Ошибка данных таблицы счетов'})

        return redirect('clients_list')

    return render(request, 'upload_files_page.html')


def ClientListView(request):
    # clients with count organizations
    clients = list(Client.objects.values("name").annotate(count=Count('organizations')))

    # total clients data
    clients_data = clients

    # clients with total summa of bills
    clients = list(Client.objects.values("name").annotate(
        summa=Sum('organizations__bills__summa')
    ))

    # union two querysets
    for client in clients_data:
        for cl in clients:
            if client['name'] == cl['name']:
                client['summa'] = cl['summa']
                break

    return render(request, 'clients_list_page.html', {
        'clients': clients_data,
    })


def BillListView(request):
    bills = Bill.objects.all()
    filter_set = BillFilter(request.GET, queryset=bills)
    return render(request, 'bills_list_page.html', {
        'bills': filter_set.qs,
        'bill_filter': filter_set,
    })


@swagger_auto_schema(
    methods=['POST', ],
    operation_description='Сохранение клиентов и организаций',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "file": openapi.Schema(description="Таблица .xlsx",
                                   type=openapi.TYPE_FILE),
        },
    ),
    responses={
        200: "ok",
    }
)
@api_view(['POST'])
def post_client_org_xlsx(request):
    client_org = request.FILES.get('file')
    if client_org:
        data = extract_data_from_client_org_excel(client_org)
        if not data or not save_client_org(data[0], data[1]):
            return Response({'error': 'Ошибка данных таблицы клиентов'}, status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_201_CREATED)


@swagger_auto_schema(
    methods=['POST', ],
    operation_description='Сохранение счетов',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            "file": openapi.Schema(description="Таблица .xlsx",
                                   type=openapi.TYPE_FILE),
        },
    ),
    responses={
        200: "ok",
    }
)
@api_view(['POST'])
def post_bills_xlsx(request):
    bills = request.FILES.get('file')
    if bills:
        data = extract_data_from_bills_excel(bills)
        if not data or not save_bills(data):
            return Response({'error': 'Ошибка данных таблицы счетов'}, status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_201_CREATED)


class ClientViewSet(viewsets.ModelViewSet):
    """Вьюшки по клиентам"""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class OrganizationViewSet(viewsets.ModelViewSet):
    """Вьюшки по организациям"""
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer


# # Не понравилось описание в сваггере, но все работает
# # Поэтому переписал api вручную
# class BillViewSet(viewsets.GenericViewSet,
#                   viewsets.mixins.ListModelMixin):
#     """Вьюшки по счетам"""
#     queryset = Bill.objects.all()
#     serializer_class = BillSerializer
#     filter_backends = [dj_filter.DjangoFilterBackend, ]
#     filter_class = BillFilter


@swagger_auto_schema(
    methods=['GET', ],
    operation_description='Получить счета',
    manual_parameters=[
        openapi.Parameter(
            'organization',
            in_=openapi.IN_QUERY,
            description='ID организации',
            type=openapi.TYPE_INTEGER,
        ),
        openapi.Parameter(
            'client',
            in_=openapi.IN_QUERY,
            description='ID клиента',
            type=openapi.TYPE_INTEGER,
        ),
    ],
    responses={
        200: BillSerializer(many=True),
    }
)
@api_view(['GET'])
def get_bills_list(request):
    bills = Bill.objects.all()
    organization_id = request.GET.get('organization')
    client_id = request.GET.get('client')

    if organization_id:
        organization = get_organization_by_id(organization_id)
        bills = bills.filter(organization=organization)

    if client_id:
        client = get_client_by_id(client_id)
        bills = bills.filter(organization__client=client)

    serializer = BillSerializer(bills, many=True)
    return Response(serializer.data)