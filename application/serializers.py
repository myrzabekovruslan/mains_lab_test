from rest_framework import serializers

from .models import Client, Bill, Organization


class ClientSerializer(serializers.ModelSerializer):
    """Сериализатор для клиента"""

    class Meta:
        model = Client
        fields = "__all__"


class OrganizationSerializer(serializers.ModelSerializer):
    """Сериализатор для организации"""

    class Meta:
        model = Organization
        fields = "__all__"


class BillSerializer(serializers.ModelSerializer):
    """Сериализатор для счета"""

    class Meta:
        model = Bill
        fields = "__all__"