# Mains Lab

Тестовое задание.

## Запуск проекта

```
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
```

## Описание

Есть 4 страницы.

- Главная (Mains Lab company)
- Загрузка файлов
- Список клиентов
- Список счетов

В каждой реализованы требуемые функции.

По url:  http://localhost:8000/swagger/ расположена документация
по rest api.
Реализованы CRUD для клиентов и организаций, а также загрузка
файлов через API и список счетов с фильтром.